package com.my.dao;

import org.springframework.stereotype.Component;

import com.my.entity.User;

@Component
public interface UserDao extends GenericDao<User>{

}
