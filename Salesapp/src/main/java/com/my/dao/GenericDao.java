package com.my.dao;

import java.util.List;

/**
 * Generic DAO contract for Persistence Operation
 *
 * @author qxk5116
 *
 * @param <T>
 */
public interface GenericDao<T> {

	/**
	 * Persist entity and returns persisted snapshot
	 *
	 * @param entity
	 *            entity to be persisted
	 * @return snapshot
	 */
	T create(final T entity);

	/**
	 * Updates entity
	 *
	 * @param entity
	 *            entity to be updated
	 * @return updated record
	 */
	T edit(final T entity);

	/**
	 * Removes entity from persistence
	 *
	 * @param entity
	 *            enity to be removed
	 */
	void remove(final T entity);

	/**
	 * Finds entity by ID
	 *
	 * @param id
	 * @return returns instance of found entity
	 */
	T findById(final Object id);

	/**
	 * Finds all records for the entity
	 *
	 * @return returns list of entities
	 */
	List<T> findAll();

	/**
	 * Finds entity records by property name
	 *
	 * @param propertyNameValuePairs
	 *            Property Name-Value pair
	 * @return returns list of entities
	 */
	List<T> findByProperty(final Object... propertyNameValuePairs);

	/**
	 * Finds entity record by property name
	 *
	 * @param propertyNameValuePairs
	 *            Property Name-Value pair
	 * @return returns record
	 */
	T findOneByProperty(final Object... propertyNameValuePairs);

	/**
	 * Finds records for pagination
	 *
	 * @param pageNumber
	 *            page number to be fetched
	 * @param pageSize
	 *            number of records to be fetched for the page
	 * @return list of records
	 */
	List<T> findRange(int pageNumber, int pageSize);

	/**
	 * Finds records by property by pagination
	 *
	 * @param pageNumber
	 *            no of page to be fetched
	 * @param pageSize
	 *            size of records within page
	 * @param propertyNameValuePairs
	 *            property Name - Value property pairs
	 * @return
	 */
	List<T> findRangeByProperty(int pageNumber, int pageSize, Object... propertyNameValuePairs);

	/**
	 * Removes records by properties.
	 *
	 * @param propertyNameValuePairs
	 *            the property name value pairs
	 * @return the int
	 */
	int removeByProperty(Object... propertyNameValuePairs);

}
