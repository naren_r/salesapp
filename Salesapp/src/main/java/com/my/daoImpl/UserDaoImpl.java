package com.my.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.my.dao.UserDao;
import com.my.entity.User;

@Component
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {


	/** The Entity Manager */
	@PersistenceContext
	private EntityManager em;

	public UserDaoImpl(){
		super(User.class);
	}
	
	public UserDaoImpl(Class<User> entityClass) {
		super(entityClass);
	}

	public List<User> findByProperty(Object... propertyNameValuePairs) {
		return null;
	}


	@Override
	protected EntityManager getEntityManager() {
		return this.em;
	}

}
