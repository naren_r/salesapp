package com.my.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.my.dao.ItemDao;
import com.my.entity.Product;

@Component
public class ItemDaoImpl extends GenericDaoImpl<Product> implements ItemDao {


	/** The Entity Manager */
	@PersistenceContext
	private EntityManager em;

	public ItemDaoImpl(){
		super(Product.class);
	}
	
	public ItemDaoImpl(Class<Product> entityClass) {
		super(entityClass);
	}

	public List<Product> findByProperty(Object... propertyNameValuePairs) {
		return null;
	}


	@Override
	protected EntityManager getEntityManager() {
		return this.em;
	}

}
