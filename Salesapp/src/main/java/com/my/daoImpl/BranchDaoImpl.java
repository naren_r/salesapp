package com.my.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.my.dao.BranchDao;
import com.my.entity.Branch;

@Component
public class BranchDaoImpl extends GenericDaoImpl<Branch> implements BranchDao {


	/** The Entity Manager */
	@PersistenceContext
	private EntityManager em;

	public BranchDaoImpl(){
		super(Branch.class);
	}
	
	public BranchDaoImpl(Class<Branch> entityClass) {
		super(entityClass);
	}

	public List<Branch> findByProperty(Object... propertyNameValuePairs) {
		return null;
	}


	@Override
	protected EntityManager getEntityManager() {
		return this.em;
	}

}
