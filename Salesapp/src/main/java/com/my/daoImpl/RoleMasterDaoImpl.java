package com.my.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.my.dao.RoleMasterDao;
import com.my.dao.UserDao;
import com.my.entity.RoleMaster;
import com.my.entity.User;

@Component
public class RoleMasterDaoImpl extends GenericDaoImpl<RoleMaster> implements RoleMasterDao {


	/** The Entity Manager */
	@PersistenceContext
	private EntityManager em;

	public RoleMasterDaoImpl(){
		super(RoleMaster.class);
	}
	
	public RoleMasterDaoImpl(Class<RoleMaster> entityClass) {
		super(entityClass);
	}

	public List<RoleMaster> findByProperty(Object... propertyNameValuePairs) {
		return null;
	}


	@Override
	protected EntityManager getEntityManager() {
		return this.em;
	}

}
