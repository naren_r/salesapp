package com.my.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.my.dao.CustomerDao;
import com.my.entity.Customer;

@Component
public class CustomerDaoImpl extends GenericDaoImpl<Customer> implements CustomerDao {


	/** The Entity Manager */
	@PersistenceContext
	private EntityManager em;

	public CustomerDaoImpl(){
		super(Customer.class);
	}
	
	public CustomerDaoImpl(Class<Customer> entityClass) {
		super(entityClass);
	}


	@Override
	protected EntityManager getEntityManager() {
		return this.em;
	}

	public List<Customer> findByProperty(Object... propertyNameValuePairs) {
		return null;
	}

}
