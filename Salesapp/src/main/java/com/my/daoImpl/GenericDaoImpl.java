package com.my.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.transaction.annotation.Transactional;

import com.my.dao.GenericDao;

/**
 * Generic DAO implementation for persistence.<br/>
 * Contains implementation for basic operation like insert, update and delete, also contains implementation for querying
 * entity by id, property
 *
 * @author qxk5116
 */
@Transactional(readOnly=false)
public abstract class GenericDaoImpl<T> implements GenericDao<T> {

	/**
	 * Entity class type holder
	 */
	private final Class<T> entityClass;

	/**
	 * Constructs Generic DAO based upon Entity Class passed
	 *
	 * @param entityClass
	 */
	public GenericDaoImpl(final Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * Contract method to expose entity manager
	 *
	 * @return returns entity manager
	 */
	protected abstract EntityManager getEntityManager();
	
	public T create(T entity) {
		this.getEntityManager().persist(entity);
		this.getEntityManager().flush();
		return this.getEntityManager().merge(entity);
	}
	
	/**
	 * @see GenericDao#edit(Object)
	 */
	public T edit(final T entity) {
		return this.getEntityManager().merge(entity);
	}

	
	/**
	 * @see GenericDao#remove(Object)
	 */
	public void remove(final T entity) {
		this.getEntityManager().remove(this.getEntityManager().merge(entity));
	}

	/**
	 * Removes by property.
	 *
	 * @param propertyNameValuePairs
	 *            the property name value pairs
	 * @return the int
	 * @see com.bmw.dealerdelivery.dao.domain.dao.GenericDao#removeByProperty(java.lang.Object[])
	 */
	public int removeByProperty(final Object... propertyNameValuePairs) {
		final TypedQuery<T> query = this.generateTypedQuery("delete", propertyNameValuePairs);
		return query.executeUpdate();
	}

	/**
	 * @see GenericDao#findById(Object)
	 */
	public T findById(final Object id) {
		if (id == null) {
			return null;
		}
		return this.getEntityManager().find(this.entityClass, id);
	}
	
	/**
	 * @see GenericDao#findAll()
	 */
	public List<T> findAll() {
		final CriteriaQuery<T> cq = this.getEntityManager().getCriteriaBuilder().createQuery(this.entityClass);
		cq.select(cq.from(this.entityClass));
		return this.getEntityManager().createQuery(cq).getResultList();
	}

	
	/**
	 * @see GenericDao#findOneByProperty(Object...)
	 */
	public T findOneByProperty(final Object... propertyNameValuePairs) {
		final TypedQuery<T> query = this.generateTypedQuery(propertyNameValuePairs);
		query.setMaxResults(1);
		final List<T> results = query.getResultList();
		if (results != null && !results.isEmpty()) {
			return results.get(0);
		}
		return null;
	}
	
	/**
	 * @see GenericDao#findRange(int, int)
	 */
	public List<T> findRange(final int pageNumber, final int pageSize) {
		final CriteriaQuery<T> cq = this.getEntityManager().getCriteriaBuilder().createQuery(this.entityClass);
		cq.select(cq.from(this.entityClass));
		final TypedQuery<T> query = this.getEntityManager().createQuery(cq);
		query.setFirstResult((pageNumber - 1) * pageSize);
		query.setMaxResults(pageSize);
		return query.getResultList();
	}

	/**
	 * @see GenericDao#findRangeByProperty(int, int, Object...)
	 */
	public List<T> findRangeByProperty(final int pageNumber, final int pageSize, final Object... properties) {
		final TypedQuery<T> query = this.generateTypedQuery(properties);
		query.setFirstResult((pageNumber - 1) * pageSize);
		query.setMaxResults(pageSize);
		return query.getResultList();
	}
	

	/**
	 * Generated TypedQuery instance based upon property Name Value Pairs
	 *
	 * If value is null then instead of = operator and value, it injects IS NULL operator
	 *
	 * @param propertyNameValuePairs
	 *            Name Value Property Pairs
	 * @return returns TypedQuery
	 */
	private TypedQuery<T> generateTypedQuery(final Object... propertyNameValuePairs) {
		return this.generateTypedQuery("select model", propertyNameValuePairs);
	}


	/**
	 * Generate typed query.
	 *
	 * @param clause
	 *            the clause //SELECT -> select model, DELETE -> delete
	 * @param propertyNameValuePairs
	 *            the property name value pairs
	 * @return the typed query
	 */
	private TypedQuery<T> generateTypedQuery(final String clause, final Object... propertyNameValuePairs) {
		final StringBuilder sbQueryString = new StringBuilder(50);
		sbQueryString.append(clause + " from " + this.entityClass.getSimpleName() + " model");
		int paramCount = 0;
		final List<Object> paramValues = new ArrayList<Object>(
				propertyNameValuePairs != null ? (propertyNameValuePairs.length / 2) : 0);
		if (propertyNameValuePairs != null) {
			for (int i = 0; i < propertyNameValuePairs.length; i += 2) {
				if (i <= 1) {
					sbQueryString.append(" where");
				} else {
					sbQueryString.append(" and");
				}
				sbQueryString.append(" model.");
				sbQueryString.append(propertyNameValuePairs[i]);
				final Object value = propertyNameValuePairs[i + 1];
				if (value != null) {
					sbQueryString.append("= :propertyValue");
					sbQueryString.append(++paramCount);
					paramValues.add(value);
				} else {
					sbQueryString.append(" IS NULL");
				}
			}
		}

		final TypedQuery<T> query = this.getEntityManager().createQuery(sbQueryString.toString(), this.entityClass);
		for (int i = 0; i < paramCount; i++) {
			query.setParameter("propertyValue" + (i + 1), paramValues.get(i));
		}

		return query;
	}

	
	}
