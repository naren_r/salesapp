package com.my.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.my.dao.BranchDao;
import com.my.dao.CustomerDao;
import com.my.dao.UserDao;
import com.my.dto.AddBranchDTO;
import com.my.dto.AddCustomerDTO;
import com.my.dto.AddUserDTO;
import com.my.entity.Branch;
import com.my.entity.Customer;
import com.my.entity.User;
import com.my.utility.RESTServiceException;

@Component
public class BranchService {

	final static Logger logger = Logger.getLogger(BranchService.class);

	@Autowired
	private CustomerDao customerDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private BranchDao branchDao;

	@Transactional
	public void addBranch(AddBranchDTO dto) throws RESTServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("addBranch() called!");
		}
		Branch branch = new Branch();
		branch.setName(dto.getName());
		branch.setCity(dto.getCity());
		this.branchDao.create(branch);
	}

	@Transactional
	public List<Branch> getAllBranch() throws RESTServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getAllBranch() called!");
		}
		return this.branchDao.findAll();
	}

	@Transactional
	public void editBranch(AddBranchDTO dto) throws RESTServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("editBranch called!");
		}
		Branch branch = this.branchDao.findOneByProperty(Branch.NAME,
				dto.getName());
		if (branch == null) {
			throw new RESTServiceException("Invalid Branch Name!",
					HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
		}
		branch.setCity(dto.getCity());
		this.branchDao.edit(branch);
	}

	@Transactional
	public void deleteBranch(AddBranchDTO dto) throws RESTServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("deleteBranch[Name = " + dto.getName()
					+ " = ] called!");
		}
		Branch branch = this.branchDao.findOneByProperty(Branch.NAME,
				dto.getName());
		if (branch == null) {
			throw new RESTServiceException("Invalid Branch Name!",
					HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null);
		}
		this.branchDao.remove(branch);
	}
}
